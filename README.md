# GSWG



## Getting Started with Git


This project uses Git for version control. Git allows you to track changes in your code, revert to previous versions, and collaborate with others.

Installation:

Before you begin, make sure you have Git installed on your system. You can download and install it from the official Git website: https://git-scm.com/
  
### CLone the repository:
  

Use the following command to clone (download) a copy of the repository to your local machine:

> git clone https://gitlab.com/89k08d23T/gswg.git


### Create Branch:

To create a new branch and switch to it at the same time, you can run the git checkout command with the -b switch:

> git checkout -b feat/jira-1234

This is shorthand for:

> git branch
> git checkout


### Stage changes:


Use the `git add` command to tell Git which files you want to include in the next commit.

> git add [ filename ]

Stage in bulk and add all changes in your next commit.

> git add [ . ] 

### Commit changes:


Use the `git commit` command to create a snapshot of the staged changes. Add a descriptive message to explain what changes you made.

> git commit -m "Added a new feature"

Use common naming convention for better collaboration

> git commit -m "jira-123/Added a new feature"


### Pull remote changes:


Before pushing your local commits, it's crucial to keep your local repository synchronized with the remote repository ( GitLab). Use the `git pull` command to fetch the latest changes from the remote branch and integrate them into your local branch:

> git pull origin main

### Push changes:


If you're working on a remote repository like GitHub, you can push your local commits to the remote branch using:

> git push origin main





## Git Integration with VS Code

-   Download and install VS Code from the official website:  [https://code.visualstudio.com/](https://code.visualstudio.com/)

### Clone the repository:
    
    Open VS Code and navigate to the desired folder where you want to clone the project. You can use the integrated Git functionality:
    
    -   Go to the  **Source Control**  tab (usually on the left sidebar).
    -   Click on the  **"Clone Repository..."**  button.
    -   Provide the URL of the remote repository (e.g., from GitHub) and choose the target directory. VS Code will handle the cloning process.
    
### Make changes:
    
    Edit the files in the project directory as needed directly within VS Code. Changes will be reflected in the Source Control view.
    
### Stage changes:
    
    -   Using the Source Control view:**  Click on the "+" icon next to the filenames you want to include 

### Commit changes:
    
    -   Using the Source Control view:**  Click on the three dots (...) next to the staged files and select "Commit."


### Pull remote changes :
    
    Before pushing your local commits, it's crucial to keep your local repository synchronized. Use the integrated functionality:
    
    -   In the Source Control view, click on the  **"Pull..."**  button.
    -   Select the remote branch you want to pull from (often  `main`  or  `master`).
    -   VS Code will fetch and merge the changes. Resolve any merge conflicts if prompted (explained later).


### Push changes (optional):
    
    After confirming you have the latest changes and resolved any conflicts, push your local commits to the remote branch:
    
    -   In the Source Control view, click on the  **"Push..."**  button.
    -   Select the remote branch you want to push to (often  `main`  or  `master`).
    -   VS Code will handle pushing your commits to the remote repository.




**Additional Resources:**

-   [https://git-scm.com/](https://git-scm.com/)  offers extensive documentation and tutorials.
-   [https://www.atlassian.com/git/tutorials](https://www.atlassian.com/git/tutorials)  provides a beginner-friendly introduction to Git.
-   [https://code.visualstudio.com/docs/sourcecontrol/overview](https://code.visualstudio.com/docs/sourcecontrol/overview) VS Code Git documentationgit